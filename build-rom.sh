#!/bin/bash

rom_fp="$(date +%y%m%d)"
mkdir -p release/$rom_fp/
set -e

if [ "$#" -le 1 ];then
	echo "Usage: $0 <android-8.1> <carbon|lineage|rr|bootleg> '# of jobs'"
	exit 0
fi
localManifestBranch=$1
rom=$2

if [[ -n "$3" ]];then
	jobs=$3
else
    if [[ $(uname -a) = "Darwin" ]];then
        jobs=$(sysctl -n hw.ncpu)
    elif [[ $(uname -a) = "Linux" ]];then
        jobs=$(nproc)
    fi
fi

if [ -d .repo/local_manifests ] ;then
	( cd .repo/local_manifests; git fetch; git reset --hard; git checkout origin/$localManifestBranch)
else
	git clone https://github.com/phhusson/treble_manifest .repo/local_manifests -b $localManifestBranch
fi

#We don't want to replace from AOSP since we'll be applying patches by hand
rm -f .repo/local_manifests/replace.xml
rm -f .repo/local_manifests/opengapps.xml

(cd device/phh/treble; git clean -fdx; bash generate.sh $rom)

. build/envsetup.sh

buildVariant() {
	lunch $1
	make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp installclean
	make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp -j$jobs systemimage
	make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp vndk-test-sepolicy
	cp $OUT/system.img GSI_BTLG/system-${2}.img
}

repo manifest -r > GSI_BTLG/manifest.xml
buildVariant treble_arm64_avN-userdebug arm64-aonly
